<?php

namespace Fanplayr\SmartAndTargeted\Controller\Connect;

class Index extends \Magento\Framework\App\Action\Action
{
    /**
     * @var \Magento\Framework\Controller\Result\JsonFactory
     */
    protected $resultPageFactory;
    /**
     * @param \Magento\Framework\App\Action\Context $context
     * @param \Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory
     */
    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory
    ) {
        parent::__construct($context);
        $this->resultJsonFactory = $resultJsonFactory;
    }
    /**
     * View  page action
     *
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {
        $helper = $this->_objectManager->create('Fanplayr\SmartAndTargeted\Helper\Data');
        $helper->init();
        $cookieName = "_fphu";

        try {
            $response = $this->getResponse();

            $params = $this->getRequest()->getParams();

            if (isset($params["data"]) && $params["data"] !== "") {
                $maxAge = 31536000;
                $encoded = urlencode($params["data"]);
                $cookieValue = "${cookieName}=${encoded}; Path=/; Max-Age=${maxAge}; HttpOnly; Secure; SameSite=Strict;";
                $response->setHeader("Set-Cookie", $cookieValue);
                return $response->setBody($params["data"]);
            } else {
                if (isset($_COOKIE[$cookieName])) {
                    return $response->setBody($_COOKIE[$cookieName]);
                }
                return $response->setBody('');
            }

        } catch (Exception $e) {
            $helper->log('Connect/execute() ERROR: ' . $e->getMessage());
            throw $e;
        }
    }
}
