<?php

namespace Fanplayr\SmartAndTargeted\Controller\Coupon;

class Session extends \Magento\Framework\App\Action\Action
{
  /**
   * @var \Magento\Framework\Controller\Result\JsonFactory
   */
  protected $result;
  protected $session;
  /**
   * @param \Magento\Framework\App\Action\Context $context
   * @param \Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory
   */
  public function __construct(
    \Magento\Framework\App\Action\Context $context,
    \Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory,
    \Magento\Catalog\Model\Session $session
  ) {
    parent::__construct($context);
    $this->result = $resultJsonFactory->create();
    $this->session = $session;
  }
  /**
   * View  page action
   *
   * @return \Magento\Framework\Controller\ResultInterface
   */
  public function execute()
  {
    // DEBUG
    $helper =$this->_objectManager->create('Fanplayr\SmartAndTargeted\Helper\Data');
    $helper->log('Session/execute() SESSION ID: ' . $this->session->getSessionId());

    $couponCode = strtoupper($this->getRequest()->getParam('coupon_code'));

    // get current valid session offers
    try {
      $sessionOffersValidated = json_decode($this->session->getData('fanplayr_session_offers'));
    } catch(\Exception $e) {
      $sessionOffersValidated = array();
    }
    if (!is_array($sessionOffersValidated)) {
      $sessionOffersValidated = array();
    }

    if (array_search($couponCode, $sessionOffersValidated) === false) {
      array_push($sessionOffersValidated, $couponCode);
    }

    $helper->log('Session/execute() VALID ' . json_encode($sessionOffersValidated));

    $this->session->setData('fanplayr_session_offers', json_encode($sessionOffersValidated));

    $helper->log('Session/execute() VALID AGAIN ' . $this->session->getData('fanplayr_session_offers'));

    $this->result->setData(['error' => false, 'method' => 'session', 'message' => 'Session Coupon Added.', 'module' => 'fanplayr', 'version' => $this->_objectManager->create('Fanplayr\SmartAndTargeted\Helper\Data')->getVersion()]);
    return $this->result;
  }
}