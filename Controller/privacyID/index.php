<?php

namespace Fanplayr\SmartAndTargeted\Controller\PrivacyID;

class Index extends \Magento\Framework\App\Action\Action
{
    /**
     * @var \Magento\Framework\Controller\Result\JsonFactory
     */
    protected $resultPageFactory;
    /**
     * @param \Magento\Framework\App\Action\Context $context
     * @param \Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory
     */
    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory
    ) {
        parent::__construct($context);
        $this->resultJsonFactory = $resultJsonFactory;
    }
    /**
     * View  page action
     *
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {   
            $privacyID = $this->_objectManager->create('Fanplayr\SmartAndTargeted\Block\PrivacyID');
            $helper = $this->_objectManager->create('Fanplayr\SmartAndTargeted\Helper\Data');
            $helper->init();
            $privacyID->siteKey = $helper->getConfig('fanplayr/privacyid/siteKey');
            $privacyID->siteSecret = $helper->getConfig('fanplayr/privacyid/siteSecret');
            $privacyID->handleRequest();
    }
}
